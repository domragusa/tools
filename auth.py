#!/usr/bin/env python3

import hmac
import base64
import time
import os
import sys

############################################
#                                          #
#   WARNING: DO NOT COMMIT YOUR SECRETS!   #
#                                          #
############################################

secrets = {
    'twitter': 'MNUWC3Y=',
    'gmail'  : 'NVXW4ZDP',
    'gitlab' : 'EEQSCII='
}

def auth(data):
    timef = (int(time.time() // 30)).to_bytes(8, 'big')
    key = base64.b32decode(data)
    buf = hmac.new(key, timef, 'sha1').digest()
    offset = buf[-1] & 0x0f
    buf = bytearray(buf[offset:offset+4])
    buf[0] &= 0x7f
    code = int.from_bytes(buf, byteorder='big') % 10**6
    return f'{code:06}'

if __name__ == '__main__':
    if len(sys.argv) < 2: sys.exit(0)
    
    name = sys.argv[1]
    if name not in secrets: sys.exit(0)
    
    code = auth(secrets[name])
    print(code)
    
    os.system(f'echo "{code}" | xclip -i -selection clipboard')
