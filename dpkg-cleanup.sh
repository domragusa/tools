#!/bin/sh

# credits to the debian wikis for the original command

RC_PKG=`dpkg -l | grep ^rc | cut -d' ' -f3`

if [ -n "$RC_PKG" ]
then
    echo "Cleaning up . . ."
    dpkg --purge $RC_PKG
else
    echo "Nothing to do."
fi

exit 0
