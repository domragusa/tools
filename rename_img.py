#!/usr/bin/env python3

import os
import PIL.Image
from datetime import datetime
#import PIL.ExifTag

def removechars(inp, chars):
    for i in chars:
        inp = inp.replace(i, '')
    return inp

files = os.listdir("./")
ids = []
for f in files:
    if not (f.lower().endswith("jpg") or f.lower().endswith("jpeg")): continue
    with PIL.Image.open(f) as img:
        exif = img._getexif()
    
    if not exif: exif = {}
    if 306 in exif: #306 = DateTime
        data = exif[306]
    elif 36867 in exif: #36867 = Date and Time (digitalized)
        data = exif[36867]
    elif 34853 in exif: #34853 = GPSInfo
        data = exif[34853]
        tmp = data[7] # fixed point numbers, X = (A, B) => X = A/B
        data = '{}{:02d}{:02d}{:02d}'.format(data[29], tmp[0][0], tmp[1][0], tmp[2][0])
        if tmp[0][1] != 1 or tmp[1][1] != 1 or tmp[2][1] != 1:
            print(f)
    else:
        data = os.path.getmtime(f)
        data = str(datetime.fromtimestamp(data))
        data = data.split(".")[0]
    
    data = removechars(data, "-.: ")
    
    c = ids.count(data)
    ids.append(data)
    if c > 0:
        data += "_"+str(c)
    
    data += ".jpeg"
    
    if os.path.isfile(data):
        print("skipping ", f)
        continue
    os.rename(f, data)
